package jtm.testing;

import jtm.testing.EvenNumbers;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EvenNumbersTest {

    @Test
    public void divisibleByTest() {

        int[] arr = new int[]{1, 2, 3, 4, 5, 6};
        int divisor = 2;
        int[] expectedResult = new int[]{2, 4, 6};
        int[] result = EvenNumbers.divisibleBy(arr, divisor);

        Assert.assertEquals(3, result.length);
        Assert.assertArrayEquals(expectedResult, result);
    }

    @Test
    public void divisibleByEmptyResultTest() {

        int[] arr = new int[]{};
        int divisor = 2;
        int[] expectedResult = new int[]{};
        int[] result = EvenNumbers.divisibleBy(arr, divisor);

        Assert.assertEquals(0, result.length);
        Assert.assertArrayEquals(expectedResult, result);
    }

    @Test
    public void divisibleByEmptyResult2Test() {

        int[] arr = new int[]{1,3,5,7,9};
        int divisor = 2;
        int[] expectedResult = new int[]{};
        int[] result = EvenNumbers.divisibleBy(arr, divisor);

        Assert.assertEquals(0, result.length);
        Assert.assertArrayEquals(expectedResult, result);
    }
}
