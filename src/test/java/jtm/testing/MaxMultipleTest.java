package jtm.testing;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MaxMultipleTest {

    MaxMultiple maxMultiple;

    @Before //Pirms katra testa izpildes
    public void setUp() {
        maxMultiple = new MaxMultiple();
    }

    @Test
    public void maxMultipleTest() {
        int result = maxMultiple.maxMultiple(37, 200);//185
        Assert.assertEquals(
                "Max multiple with divisor 37 and bound 200 expected to be 185",
                185, result);
    }

    @Test
    public void maxMultipleSmallNumbersTest() {
        int result = maxMultiple.maxMultiple(2, 7);//6
        Assert.assertEquals(6, result);
    }

    @Test
    public void maxMultipleNoResultTest() {
        int result = maxMultiple.maxMultiple(9, 7);//6
        Assert.assertEquals(-1, result);
    }
}
